﻿using System.Collections.Generic;
using Assets.Helpers;
using UnityEngine;

public class GameObjectPull<T> where T : MonoBehaviour
{
    private GameObject Container;
    private T prefab;
    private Stack<T> instances;

    public void Init(GameObject container, T prefabObj, int count)
    {
        prefab = prefabObj;
        Container = container;
        instances = new Stack<T>(count);
        for (int i = 0; i < count; i++)
        {
            instances.Push(PrefabHelper.Intantiate(prefab, Container));   
        }
    }

    public T GetObject()
    {
        return instances.Pop();
    }

    public void ReleaseObject(T objectToRelease)
    {
        objectToRelease.gameObject.AppendTo(Container);
        instances.Push(objectToRelease);
    }

    public void ClearPull()
    {
        while (instances.Count > 0)
        {
            GameObject.Destroy(instances.Pop());
        }
    }
}
