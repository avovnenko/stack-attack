﻿using System.Collections.Generic;
using System.Net.NetworkInformation;
using Assets.Helpers;
using UnityEngine;

public class TransferManager : MonoBehaviour
{
    public BlockTransfer TransferPrefab;
    public Block BlockPrefab;
    public GameObject BlocksContainer;
    public GameObject PullContainer;

    private const int ColsCount = 10;
    private const float BlockWidth = 64;
    private const float Delay = 10;
    private float nextTransferTime = 2;
    private List<BlockTransfer> transfers = new List<BlockTransfer>();
    private GameObjectPull<BlockTransfer> transfersPull = new GameObjectPull<BlockTransfer>();
    private GameObjectPull<Block> blocksPull = new GameObjectPull<Block>();

    private void Start()
    {
        transfersPull.Init(PullContainer, TransferPrefab, 10);
        blocksPull.Init(PullContainer, BlockPrefab, 100);
    }

    private void Update()
    {
        nextTransferTime -= Time.deltaTime;

        for (int i = 0; i < transfers.Count;)
        {
            var blockTransfer = transfers[i];
            if (blockTransfer.transform.localPosition.x < -BlockWidth ||
                blockTransfer.transform.localPosition.x > BlockWidth*(ColsCount + 1))
            {
                transfers.RemoveAt(i);
                transfersPull.ReleaseObject(blockTransfer);
            }
            else
            {
                i++;
            }
        }

        if (nextTransferTime <= 0)
        {
            nextTransferTime = Delay;
            transfers.Add(CreateTransfer());
        }
    }

    private BlockTransfer CreateTransfer()
    {
        BlockTransfer transfer = transfersPull.GetObject();
        
        transfer.gameObject.AppendTo(gameObject);
        transfer.BlocksContainer = BlocksContainer;
        transfer.Destination = BlockWidth/2 + Random.Range(0, ColsCount) * BlockWidth;
        bool fromRightToLeft = Random.Range(-1f, 1f) > 0;
        transfer.transform.localPosition = new Vector3(fromRightToLeft ? BlockWidth*ColsCount + BlockWidth/2 : -BlockWidth/2, 0);
        transfer.Velocity = (fromRightToLeft ? -1 : 1) * 5;

        Block block = blocksPull.GetObject();
        block.gameObject.AppendTo(transfer.gameObject);
        block.transform.localPosition = Vector3.zero;
        block.rigidbody2D.gravityScale = 0;
        block.collider2D.enabled = false;
        transfer.InnerBlock = block;
            
        return transfer;
    }
}