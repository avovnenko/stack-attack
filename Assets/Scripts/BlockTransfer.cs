﻿using System;
using UnityEngine;

public class BlockTransfer : MonoBehaviour
{
    public Block InnerBlock;
    public float Velocity;
    public float Destination;
    public GameObject BlocksContainer;

    // Update is called once per frame
    void Update()
    {
        if (InnerBlock != null && Math.Abs(transform.localPosition.x - Destination) < Math.Abs(Velocity))
        {
            ThrowBlock();    
        }
        else
        {
            transform.localPosition += new Vector3(Velocity, 0);
        }
    }


    private void ThrowBlock()
    {
        InnerBlock.transform.parent = BlocksContainer.transform;
        InnerBlock.transform.localScale = Vector3.one;

        InnerBlock.rigidbody2D.gravityScale = 1;
        InnerBlock.collider2D.enabled = true;
        InnerBlock = null;
    }
}