﻿using UnityEngine;

namespace Assets.Helpers
{
    public static class PrefabHelper
    {
        public static T Intantiate<T>(T prefab, GameObject parent) where T : MonoBehaviour
        {
            T instance = Object.Instantiate(prefab) as T;
            if (instance == null) return null;

            instance.gameObject.AppendTo(parent);
        
            return instance;
        }

        public static void AppendTo(this GameObject go, GameObject parent)
        {
            go.transform.parent = parent.transform;
            go.transform.localScale = Vector3.one;
            go.gameObject.layer = parent.layer;
        }
    }
}
