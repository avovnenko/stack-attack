﻿using System;
using UnityEngine;
using System.Collections;

public enum WorkerState
{
    Stand,
    GoRight,
    GoLeft,
    Jump
}

public class Worker : MonoBehaviour
{
    public SpriteRenderer Sprite;
    public Rigidbody2D Rigidbody;

    private WorkerState currentState = WorkerState.Stand;

	// Update is called once per frame
	void Update () {
	    if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow))
	    {
            currentState = WorkerState.GoLeft;
            Rigidbody.velocity = new Vector2(-1, 0);
	    } else if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow))
	    {
	        currentState = WorkerState.GoRight;
            Rigidbody.velocity = new Vector2(1, 0);
	    }
        else if ((Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.UpArrow))
            && Math.Abs(Rigidbody.velocity.y) < float.Epsilon)
        {
            currentState = WorkerState.Jump;
            Rigidbody.AddForce(new Vector2(0, 1), ForceMode2D.Impulse);
        }
        else if (Math.Abs(Rigidbody.velocity.y) < float.Epsilon)
        {
            currentState = WorkerState.Stand;
            Rigidbody.velocity = Vector2.zero;
        }
	}
}
